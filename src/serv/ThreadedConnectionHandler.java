package serv;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.bluetooth.RemoteDevice;
import javax.microedition.io.StreamConnection;

import serv.commands.*;
import serv.scans.*;

/**
 *
 * @author Brent Jacobs
 */
public class ThreadedConnectionHandler extends Thread {

    // client/server details
    Server server;
    StreamConnection connection;
    
    // in/out streams
    private BufferedReader in; 
    private PrintWriter out; 
    
    // device details
    String device_name, device_address;
    
    // currently running commands
    
    
    public ThreadedConnectionHandler(Server server, StreamConnection connection) throws IOException  {
        this.server = server;
        this.connection = connection;
        
        // get in/out streams
        in = new BufferedReader(new InputStreamReader(connection.openInputStream()));
        out = new PrintWriter(new BufferedOutputStream(connection.openOutputStream()));

        // get device details
        RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
        device_name = dev.getFriendlyName(false); // false means the device is not asked directly, but rather use data already sent
        device_address = dev.getBluetoothAddress();
    }
    
    @Override
    public void run() {
        
        System.out.println("** connected to " + device_name + " (" + device_address + ")");
        
        try {
            // process request
            String request;
            while ((request = in.readLine()) != null) {
                handle_request(request);
            }
            
            // close all streams and connections
            in.close();
            out.close();
            connection.close();
            // TODO: thread also needs to be removed from handlers
            
            System.out.println("** connection to " + device_name + " (" + device_address + ") closed");
            
        } catch (IOException ex) {
            System.out.println("!! Error reading request from " + device_name + " (" + device_address + ")");
            Logger.getLogger(ThreadedConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void handle_request(String request) {
        System.out.println("** received request from " + device_name + " (" + device_address + "): " + request);
        
        Scanner scanner = new Scanner(request);
        String command = scanner.next();
        
        Command cmd;
        System.out.println("[DEBUG] command: " + command);
        switch (command.toLowerCase()) {
            case "scannetwork":
                System.out.println("[DEBUG] scannetwork case");
                cmd = new ScanCommand(new NetworkScan(out));
                break;
//            case "portscan":
//                System.out.println("[DEBUG] portscan case");
//                String inetAddress = scanner.next();
//                cmd = new ScanCommand(new PortScan(out, inetAddress));
//                break;
            case "portscan":
                System.out.println("[DEBUG] portscan case");
                String inetAddress = scanner.next();
                cmd = new ScanCommand(new VulnerabilityScan(out, inetAddress));
                break;
            case "testtext":
                System.out.println("[DEBUG] testtext case");
                cmd = new NullCommand();
                break;
            default:
                System.out.println("[DEBUG] default case");
                cmd = new ScanCommand(new NullScan(out, command));
                break;
        }
        
        System.out.println("** beginning command from " + device_name + " (" + device_address + "): " + command);
        cmd.run();
        System.out.println("** completed command from " + device_name + " (" + device_address + "): " + command);
    }
}

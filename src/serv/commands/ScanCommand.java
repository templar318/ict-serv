package serv.commands;

import serv.scans.Scan;

/**
 *
 * @author Brent Jacobs
 */
public class ScanCommand implements Command {

    Scan scan;
    
    public ScanCommand(Scan scan) {
        this.scan = scan;
    }
    
    @Override
    public void run() {
        this.scan.scan();
    }
    
}

package serv.commands;

/**
 *
 * @author Brent Jacobs
 */
public interface Command {
    public void run();
}

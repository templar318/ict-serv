package serv.commands;

/**
 *
 * @author Brent Jacobs
 */
public class ThreadedCommand extends Thread {
    
    Command command;
    
    public ThreadedCommand(Command command) {
        this.command = command;
    }
    
    @Override
    public void run() {
        // run the actual scan
        this.command.run();
    }
}

package serv.scans;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Brent Jacobs
 */
public abstract class Scan {
    
    protected PrintWriter writer;
    
    public Scan(PrintWriter writer) {
        this.writer = writer;
    }
    
    public abstract void scan();

    protected String getInetAddress(String line) {
        String inet = null;
        
        String ipaddressPattern = "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

        // find lines with inet addr
        String[] tokens = line.trim().split("inet addr:");
        String token = tokens.length == 2 ? tokens[1] : null;

        // extract inet addr
        tokens = token != null ? token.split("\\s+", 2) : null;
        token = tokens != null && tokens.length == 2 ? tokens[0] : null;

        if (token != null && token.matches(ipaddressPattern)) {
            inet = token;
        }
        
        return inet;
    }
    
    protected String getSubnetAddress(String ipaddress) {
        int i = ipaddress.lastIndexOf(".");
        return ipaddress.substring(0, i+1) + "*";
    }
    
    protected void println(String str) {
        System.out.println(str);
    }
    
    protected void printRaw(String str) {
        writer.println(str);
        writer.flush();
    }
    
    protected void printTitle(String str) {
        String padding = " ==================================================";
        str = str + padding.substring(0, Math.max(3, padding.length() - str.length()));  // pad string with atleast the first three chars
        println("=== " + str);
    }
    
    protected void printSubsection(String str) {
        String padding = " --------------------------------------------------";
        str = str + padding.substring(0, Math.max(3, padding.length() - str.length()));  // pad string with atleast the first three chars
        println("--- " + str);
    }
    
    protected void printMsg(String str) {
        println("** " + str);
    }
    
}

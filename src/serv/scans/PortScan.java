package serv.scans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.EnumSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * See: http://nmap.org/book/man-port-scanning-techniques.html
 *
 * === Scans =======
 * -sS (TCP SYN scan)            - 4.00 sec
 * -sT (TCP connect scan)        - 6.34 sec
 * -sU (UDP scan)               - way to long, over 5? mins
 * -sY (SCTP INIT scan)          - 3.49 sec - didn't show anything useful
 * -sN (TCP NULL scan)           - 7.26 sec
 * -sF (TCP FIN scan)            - 7.31 sec
 * -sX (TCP Xmas scan)           - 5.52 sec
 * -sA (TCP ACK scan)            - 6.77 sec - didn't show anything useful
 * -sW (TCP Window scan)         - 4.39 sec - didn't show anything useful
 * -sM (TCP Maimon scan)         - 5.92 sec - didn't show anything useful
 * -sZ (SCTP COOKIE ECHO scan)   - 3.43 sec - returned different ports to the rest
 * -sO (IP protocol scan)        - way to long, over 5? mins
 *
 * === Sample Output =======
 * Starting Nmap 6.00 ( http://nmap.org ) at 2014-09-16 23:37 CST
 * Nmap scan report for ReadyNAS.BigPond (10.0.0.7)
 * Host is up (0.011s latency).
 * Not shown: 990 closed ports
 * PORT      STATE         SERVICE
 * 80/tcp    open|filtered http
 * 111/tcp   open|filtered rpcbind
 * 139/tcp   open|filtered netbios-ssn
 * 443/tcp   open|filtered https
 * 445/tcp   open|filtered microsoft-ds
 * 631/tcp   open|filtered ipp
 * 873/tcp   open|filtered rsync
 * 8086/tcp  open|filtered d-s-n
 * 8200/tcp  open|filtered trivnet1
 * 50000/tcp open|filtered ibm-db2
 * MAC Address: 2C:B0:5D:BF:02:83 (Netgear)
 * 
 * Nmap done: 1 IP address (1 host up) scanned in 7.26 seconds
 * 
 * === Sample Output - Host down =======
 * Starting Nmap 6.00 ( http://nmap.org ) at 2014-09-17 18:09 CST
 * Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
 * Nmap done: 1 IP address (0 hosts up) scanned in 1.90 seconds
 * 
 * @author Brent Jacobs
 */
public class PortScan extends Scan {
    
    public enum ScanType {
        TCP_SYN         ("TCP SYN scan", "sS"),
        TCP_CONNECT     ("TCP connect scan", "sT"),
        UDP             ("UDP scan", "sU"),
        SCTP_INIT       ("SCTP INIT scan", "sY"),
        TCP_NULL        ("TCP NULL scan", "sN"),
        TCP_FIN         ("TCP FIN scan", "sF"),
        TCP_XMAS        ("TCP Xmas scan", "sX"),
        TCP_ACK         ("TCP ACK scan", "sA"),
        TCP_WINDOW      ("TCP Window scan", "sW"),
        TCP_MAIMON      ("TCP Maimon scan scan", "sM"),
        SCTP_COOKIE_ECHO("SCTP COOKIE ECHO", "sZ"),
        IP_PROTOCOL     ("IP protocol scan", "sO");
        
        public final String name;
        public final String param;
        
        ScanType(String name, String param) {
            this.name = name;
            this.param = param;
        }
    }
    
    private final EnumSet<ScanType> scanTypes;
    private final String ipAddress;

    public PortScan(PrintWriter writer, String ipAddress) {
        super(writer);
        scanTypes = EnumSet.of(
            ScanType.TCP_SYN,
            ScanType.SCTP_COOKIE_ECHO
        );
        this.ipAddress = ipAddress;
    }

    @Override
    public void scan() {
        try {
            printTitle("Beginning port scan");
            printSubsection("Scanning " + ipAddress);
            printRaw("Scanning " + ipAddress + ":");
            
            String scanParams = "";
            for (ScanType scan : scanTypes) {
                scanParams += scanParams.length() == 0 ? "-" + scan.param :  " -" + scan.param;
            }
            
            // run scan and display output as xml
//            String nmapCmd = String.format("sudo nmap %s %s -oX -", scanParams, ipAddress);
            String nmapCmd = String.format("sudo nmap %s %s", scanParams, ipAddress);
            
            printMsg("executing `" + nmapCmd + "`:");
            
            Process nmapProc = Runtime.getRuntime().exec(nmapCmd);
            BufferedReader nmapProcReader = new BufferedReader(new InputStreamReader(nmapProc.getInputStream()));
            
            // read output of nmap command
            String line, xml = "";
            while ((line = nmapProcReader.readLine()) != null) {
                println(line);
//                xml += line + "\n";
                printRaw(line);
            }
            nmapProc.waitFor();
//            printRaw(xml); // TODO: don't just dump the xml
            
            
            printTitle("Port scan complete for ip address `" + ipAddress + "`: " + nmapProc.exitValue());
            nmapProc.destroy();
            
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(PortScan.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        printRaw("\nScan complete!");
        printRaw("endresults");
    }
    
}

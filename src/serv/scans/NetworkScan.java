/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serv.scans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;

/**
 *
 * @author Brent Jacobs
 */
public class NetworkScan extends Scan {
    
    public NetworkScan(PrintWriter writer) {
        super(writer);
    }
    
    @Override
    public void scan() {
        
//        TODO: use classes such as:
//                - InetAddress - http://docs.oracle.com/javase/7/docs/api/java/net/InetAddress.html
//                - NetworkInterface - http://docs.oracle.com/javase/7/docs/api/java/net/NetworkInterface.html
//                - InterfaceAddress - http://docs.oracle.com/javase/7/docs/api/java/net/InterfaceAddress.html
        
        // perform network discovery stuff
        try {
            // discover ip addresses
            printTitle("Discovering available network connections...");
            printSubsection("Discovering available network connections...");
            
            String ifconfigCmd = "ifconfig";
            printMsg("executing `" + ifconfigCmd + "`");

            Process ifconfigProc = Runtime.getRuntime().exec(ifconfigCmd);
            BufferedReader ifconfigProcReader = new BufferedReader(new InputStreamReader(ifconfigProc.getInputStream()));

            // get subnets from ifconfig cmd
            HashSet<String> subnets = new HashSet<>();
            String line;
            while ((line = ifconfigProcReader.readLine()) != null) {
                String subnet = getInetAddress(line);
                if (subnet != null && !subnet.equals("127.0.0.1")) { subnets.add(getSubnetAddress(subnet)); }
            }

            ifconfigProc.waitFor();
            printMsg("ifconfig complete: " + ifconfigProc.exitValue());
            ifconfigProc.destroy();

            if (subnets.isEmpty()) {
                printMsg("No active network connections found.");
                printRaw("No active network connections found.");
                return;
            }

            // display available subnets to mobile device and screen
            printMsg("Available subnets");
            printRaw("Available subnets:");
            for (String subnet : subnets) {
                printMsg(subnet);
                printRaw(subnet);
            }

            // search each subnet
            printTitle("Scanning network connections...");
            printRaw("Scanning network connections...");
            for (String subnet : subnets) {
                printSubsection("Scanning " + subnet);
                printRaw("Scanning " + subnet + ":");
                
                String nmapCmd = "nmap -sP " + subnet;
                printMsg("executing `" + nmapCmd + "`:");

                Process nmapProc = Runtime.getRuntime().exec(nmapCmd);
                BufferedReader nmapProcReader = new BufferedReader(new InputStreamReader(nmapProc.getInputStream()));

                // read and reply with output of nmap command 
                String string;
                while ((string = nmapProcReader.readLine()) != null) {
                    println(string);
                    printRaw(string);
                }

                nmapProc.waitFor();
                printMsg("Scan complete for subnet `" + subnet + "`: " + nmapProc.exitValue());
                nmapProc.destroy();
            }

        }
        catch (IOException | InterruptedException e) {
            System.out.println("!!! an error ocurred !!!");
            System.out.println(e.getMessage());
        }
        
        printRaw("\nScan complete!");
        printRaw("endresults");
    }
}

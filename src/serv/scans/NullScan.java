package serv.scans;

import java.io.PrintWriter;

/**
 *
 * @author Brent Jacobs
 */
public class NullScan extends Scan {

    private String scanType;
    
    public NullScan(PrintWriter writer, String scanType) {
        super(writer);
        this.scanType = scanType;
    }
    
    @Override
    public void scan() {
        printRaw("Scan type not recognised: " + scanType);
    }
    
}

package serv;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Brent
 */
public class Test {
    
    public static void main(String... args) {
        xmlParsingTest();
    }
    
    public static void regexTest() {
        String test = "Host: 10.0.0.7 (ReadyNAS.BigPond)";
        Pattern pattern = Pattern.compile("Host: (.*) \\((.*)\\)");
        Matcher matcher = pattern.matcher(test);
        
        boolean found = false;
        while (matcher.find()) {
            System.out.format("I found the text" +
                " \"%s\" starting at " +
                "index %d and ending at index %d.%n",
                matcher.group(2),
                matcher.start(),
                matcher.end());
            found = true;
        }
        
        if(!found){
            System.out.format("No match found.%n");
        }
    }
    
    public static void formatTest() {
        String nmapCmd = String.format("nmap -%s %s", "sS", "10.0.0.7");
        System.out.println(nmapCmd);
    }
    
    public static void splitTest() {
        String test = "Host: 10.0.0.7 (ReadyNAS.BigPond)	Status: Up";
        String[] arr = test.split("Host: ");
        for (String str : arr) System.out.println(str);
    }
    
    
    public static void xmlParsingTest() {
        try {
            String xml =    "<?xml version=\"1.0\"?>\n" +
                            "<?xml-stylesheet href=\"file:///usr/bin/../share/nmap/nmap.xsl\" type=\"text/xsl\"?>\n" +
                            "<!-- Nmap 6.00 scan initiated Wed Sep 17 22:41:45 2014 as: nmap -sS -oX - 10.0.0.7 -->\n" +
                            "<nmaprun scanner=\"nmap\" args=\"nmap -sS -oX - 10.0.0.7\" start=\"1410959505\" startstr=\"Wed Sep 17 22:41:45 2014\" version=\"6.00\" xmloutputversion=\"1.04\">\n" +
                            "	<scaninfo type=\"syn\" protocol=\"tcp\" numservices=\"1000\" services=\"1,3-4,6-7,9,13,17,19-26,30,32-33,...\"/>\n" +
                            "	<verbose level=\"0\"/>\n" +
                            "	<debugging level=\"0\"/>\n" +
                            "	<host starttime=\"1410959505\" endtime=\"1410959506\">\n" +
                            "		<status state=\"up\" reason=\"arp-response\"/>\n" +
                            "		<address addr=\"10.0.0.7\" addrtype=\"ipv4\"/>\n" +
                            "		<address addr=\"2C:B0:5D:BF:02:83\" addrtype=\"mac\" vendor=\"Netgear\"/>\n" +
                            "		<hostnames>\n" +
                            "			<hostname name=\"ReadyNAS.BigPond\" type=\"PTR\"/>\n" +
                            "		</hostnames>\n" +
                            "		<ports>\n" +
                            "			<extraports state=\"closed\" count=\"990\">\n" +
                            "				<extrareasons reason=\"resets\" count=\"990\"/>\n" +
                            "			</extraports>\n" +
                            "			<port protocol=\"tcp\" portid=\"80\">\n" +
                            "				<state state=\"open\" reason=\"syn-ack\" reason_ttl=\"64\"/>\n" +
                            "				<service name=\"http\" method=\"table\" conf=\"3\"/>\n" +
                            "			</port>\n" +
                            "			<port protocol=\"tcp\" portid=\"111\">\n" +
                            "				<state state=\"open\" reason=\"syn-ack\" reason_ttl=\"64\"/>\n" +
                            "				<service name=\"rpcbind\" method=\"table\" conf=\"3\"/>\n" +
                            "			</port>\n" +
                            "		</ports>\n" +
                            "		<times srtt=\"1955\" rttvar=\"586\" to=\"100000\"/>\n" +
                            "	</host>\n" +
                            "	<runstats>\n" +
                            "		<finished time=\"1410959506\" timestr=\"Wed Sep 17 22:41:46 2014\" elapsed=\"2.86\" summary=\"Nmap done at Wed Sep 17 22:41:46 2014; 1 IP address (1 host up) scanned in 2.86 seconds\" exit=\"success\"/>\n" +
                            "		<hosts up=\"1\" down=\"0\" total=\"1\"/>\n" +
                            "	</runstats>\n" +
                            "</nmaprun>";
            
            // parse string as xml document
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = db.parse(new InputSource(new StringReader(xml)));
            
            Element root = document.getDocumentElement();
            System.out.println("Document Element: " + root.getTagName());
            
            NodeList nodeList = document.getElementsByTagName("host");
            for (int x=0,size= nodeList.getLength(); x<size; x++) {
                System.out.println(nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue());
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

package serv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.bluetooth.*;
import javax.microedition.io.*;

public class Server {
    
    private ArrayList<ThreadedConnectionHandler> handlers = new ArrayList<>();
	
    public void startServer() throws BluetoothStateException, IOException {

        UUID uuid = new UUID("1101", true);
        String connectionString = "btspp://localhost:" + uuid+";name=server";

        StreamConnectionNotifier streamConnNotifier = (StreamConnectionNotifier) Connector.open(connectionString);
        System.out.println("=== Server Started ===========================");

        LocalDevice localDevice = LocalDevice.getLocalDevice();
        System.out.println("Address: "+localDevice.getBluetoothAddress());
        System.out.println("Name: "+localDevice.getFriendlyName());
        
        // continuously accept connections and spawn new handlers
        while(true) {
            try {
                System.out.println("Waiting for client to connect...");
                StreamConnection connection = streamConnNotifier.acceptAndOpen(); // blocks

                System.out.println("Device connecting...");
                // (new ConnectionHandlerThread(this, connection)).start();
                ThreadedConnectionHandler handler = new ThreadedConnectionHandler(this, connection);
                handlers.add(handler);
                handler.start();
            } catch (IOException ex) {
                // unable to accept connection
                System.out.println("!! Unable to establish connection with a client");
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        // TODO: if server closes, close all handler connections
        // TODO: add method to shut down server
        // TODO: remove handler once connection is closed

    }

    public static void main(String[] args)
    {
        try {
            Server serv=new Server();
            serv.startServer();
        }
        catch (IOException e) {
            System.out.println("=== Server stoped unexpectedly ===============");
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }
}


